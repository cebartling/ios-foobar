//
//  ContentView.swift
//  ios-foobar
//
//  Created by Bartling, Chris on 11/27/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, Foobar!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
