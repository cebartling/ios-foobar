//
//  ios_foobarApp.swift
//  ios-foobar
//
//  Created by Bartling, Chris on 11/27/20.
//

import SwiftUI

@main
struct ios_foobarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
