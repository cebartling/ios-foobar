# GitLab CI for iOS project spike solution

Test project for understanding how to use GitLab CI with iOS projects.

## Introduction


## Implementation

### Verify your Xcode installation


`ls /Applications/Xcode.app/Contents/Developer`

`sudo xcode-select -s /Applications/Xcode.app/Contents/Developer`


### Install dependencies

The following two dependencies will be needed for this implementation. The first dependency, `xcpretty`, is a `xcodebuild` output formatter. More information can be found at https://github.com/xcpretty/xcpretty.

```bash
gem install xcpretty
```

The second dependency, `gitlab-runner`, is the build agent for GitLab. This is main component for turning a macOS system into a GitLab build worker. 
I used Homebrew to install this dependency. More information can be found at https://docs.gitlab.com/runner/.

```bash
brew install gitlab-runner
```


### Create iOS project and import it into GitLab


Create a new iOS project in Xcode.

![Choosing the type of project](./images/Screen%20Shot%202020-11-30%20at%2012.06.34%20PM.png)

![Entering the project metadata](./images/Screen%20Shot%202020-11-30%20at%2012.06.56%20PM.png)

Generate a new `.gitignore` file in the root directory of the project/local repo. 

```bash
curl -o .gitignore https://www.toptal.com/developers/gitignore/api/swift
```

Add and commit the new `.gitignore` file to your git repo.

At the moment, all you have is a local repo. You need to allow GitLab access to this repo. Follow the instructions for making a local repository remote-aware.

https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-a-remote-repository


### Ensure git credential helper configuration

If you installed git through Homebrew, the credential helper is configured as `osxkeychain`. You can verify the git credential helper by using the following command:

```bash
git config credential.helper
```

If the credential helper is set to `osxkeychain`, comment out the helper line in the `/usr/local/etc/gitconfig` file (do this using `sudo`).


### Configure GitLab Runner on macOS

The `gitlab-runner` was installed via Homebrew previously. Now we need to run the `gitlab-runner` process as a daemon. Again, we'll use Homebrew to do that:

```bash 
brew services start gitlab-runner
```
Now to turn our attention to GitLab CI settings. We need to associate the registered GitLab runner to the project repository. Navigate to Settings -> CI/CD in the project repository and scroll down to the Runners configuration. Expand that section. You should see something like the following:

![CI/CD specific runner configuration](./images/Screen%20Shot%202020-11-30%20at%202.45.38%20PM.png)

You will use the URL and registration token during the following gitlab-runner registration process. Once the `gitlab-runner` process is running, register the build worker with GitLab and verify the registration:

```bash
gitlab-runner register
gitlab-runner verify
```

You should see something like the following output:

```
Runtime platform                                    arch=amd64 os=darwin pid=71915 revision=8fa89735 version=13.6.0
WARNING: Running in user-mode.
WARNING: Use sudo for system-mode:
WARNING: $ sudo gitlab-runner...

Verifying runner... is alive                        runner=ojkfsFS7
```

The settings UI should also update when successful registration is completed.


### Configure iOS project for CI

So the project builds and the test run locally. The next step is to configure this iOS project for GitLab CI.

1. Create the YAML file for GitLab in the iOS project root directory: `touch .gitlab-ci.yml`
2. Determine which device you will run the tests in the iOS Simulator: `xcrun xctrace list devices`
3. Update the destination string in the `.gitlab-ci.yml` for your desired device. I went with `'platform=iOS Simulator,name=iPad (8th generation),OS=14.2'` for this example.

See the [.gitlab-ci.yml](./.gitlab-ci.yml) for this project for the final state of this file.


### Test `xcodebuild` commands locally

Before we trigger the build through GitLab CI, make sure the `xcodebuild` commands are correct as written in the `.gitlab-ci.yml` file. Run each command from the comamnd line.

```bash
xcodebuild clean -project ios-foobar.xcodeproj -scheme ios-foobar | xcpretty
```

```bash
xcodebuild test -project ios-foobar.xcodeproj -scheme ios-foobar -destination 'platform=iOS Simulator,name=iPad (8th generation),OS=14.2' | xcpretty -s
```

### Test the GitLab CI build


